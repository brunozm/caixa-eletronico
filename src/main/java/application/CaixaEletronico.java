package application;

import domain.OperacoesCaixaEletronico;
import domain.RelatoriosCaixaEletronico;

public class CaixaEletronico {
	
	public OperacoesCaixaEletronico onOperacoes() {
		return new OperacoesCaixaEletronico();
	}
	
	public RelatoriosCaixaEletronico onRelatorios() {
		return new RelatoriosCaixaEletronico();
	}

}
