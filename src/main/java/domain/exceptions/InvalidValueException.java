package domain.exceptions;

public class InvalidValueException extends RuntimeException {

	private static final long serialVersionUID = 3533558742535810237L;

	public InvalidValueException(String message) {
		super(message);
	}

}
