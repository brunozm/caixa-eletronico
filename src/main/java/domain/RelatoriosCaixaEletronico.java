package domain;

import java.util.List;

import infrastructure.CaixaEletronicoDAO;

public class RelatoriosCaixaEletronico {
	
	private CaixaEletronicoDAO caixaEletronicoDAO = new CaixaEletronicoDAO();
	
	public List<Saque> listSaques() {
		return caixaEletronicoDAO.listSaques();
	}
	
	public List<Cedula> listTotalCedulasRetiradas() {
		return caixaEletronicoDAO.listTotalCedulasRetiradas();
	}
	
	public Integer getMediaDeSaques() {
		return caixaEletronicoDAO.getMediaDeSaques();
	}

}
