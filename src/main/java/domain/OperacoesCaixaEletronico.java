package domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import domain.exceptions.InvalidValueException;
import infrastructure.CaixaEletronicoDAO;

public class OperacoesCaixaEletronico {
	
	private CaixaEletronicoDAO caixaEletronicoDAO = new CaixaEletronicoDAO();
	
	public Saque retirar(Integer valor) {
		
		validaValor(valor);

		Integer valorRestante = valor;
		Integer ultimoDigito = valor % 10;
		
		List<Cedula> cedulas = new ArrayList<Cedula>();
		
		if(ultimoDigito == 1) {
			adicionaCedulaNaLista(cedulas, Cedula.DOIS, 3);
			adicionaCedulaNaLista(cedulas, Cedula.CINCO, 1);
			valorRestante -= 11;
		} else if (ultimoDigito == 3) {
			adicionaCedulaNaLista(cedulas, Cedula.DOIS, 4);
			adicionaCedulaNaLista(cedulas, Cedula.CINCO, 1);
			valorRestante -= 13;
		} else if (ultimoDigito == 6) {
			adicionaCedulaNaLista(cedulas, Cedula.DOIS, 3);
			valorRestante -= 6;
		} else if (ultimoDigito == 8) {
			adicionaCedulaNaLista(cedulas, Cedula.DOIS, 4);
			valorRestante -= 8;
		}
		
		while (valorRestante != 0) {
			
			if (valorRestante >= 100) {
				adicionaCedulaNaLista(cedulas, Cedula.CEM, 1);
				valorRestante -= 100;
				continue;
			} else if (valorRestante >= 50) {
				adicionaCedulaNaLista(cedulas, Cedula.CINQUENTA, 1);
				valorRestante -= 50;
				continue;
			} else if (valorRestante >= 20) {
				adicionaCedulaNaLista(cedulas, Cedula.VINTE, 1);
				valorRestante -= 20;
				continue;
			} else if (valorRestante >= 10) {
				adicionaCedulaNaLista(cedulas, Cedula.DEZ, 1);
				valorRestante -= 10;
				continue;
			} else if (valorRestante >= 5) {
				adicionaCedulaNaLista(cedulas, Cedula.CINCO, 1);
				valorRestante -= 5;
				continue;
			} else {
				adicionaCedulaNaLista(cedulas, Cedula.DOIS, 1);
				valorRestante =- 2;
			}
		}
		
		Saque saque = new Saque(new Date(), valor, cedulas);
		caixaEletronicoDAO.save(saque);
		
		return saque;
	}
	
	private void adicionaCedulaNaLista(List<Cedula> cedulas, Cedula cedula, Integer quantidade) {
		for (int i = 0; i < quantidade; i++) {
			cedulas.add(cedula);
		}
	}
	
	private void validaValor(Integer valor) {
		
		if(valor <= 3 || valor > 1000) {
			throw new InvalidValueException("Impossível sacar o valor: " + valor);
		}
		
	}

}
