package domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "SAQUES")
@Table(name = "SAQUES")
public class Saque {
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "DATA", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date data;
	
	@Column(name = "VALOR", nullable = false)
	private Integer valor;
	
	@ElementCollection(targetClass = Cedula.class)
	@Enumerated(EnumType.STRING)
	@CollectionTable(name = "SAQUE_CEDULAS")
	@Column(name = "CEDULAS")
	private List<Cedula> cedulas;
	
	public Saque(Date data, Integer valor, List<Cedula> cedulas) {
		this.data = data;
		this.valor = valor;
		this.cedulas = cedulas;
	}
	
	public Saque() {}

	public Integer getId() {
		return id;
	}

	public Date getData() {
		return data;
	}

	public Integer getValor() {
		return valor;
	}

	public List<Cedula> getCedulas() {
		return cedulas;
	}

}
