package domain;

public enum Cedula {
	
	DOIS(2), CINCO(5), DEZ(10), VINTE(20), CINQUENTA(50), CEM(100);
	
	public Integer valor;

	private Cedula(Integer valor) {
		this.valor = valor;
	}

	public Integer getValor() {
		return valor;
	}
	
}
