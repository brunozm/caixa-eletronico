package infrastructure;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import domain.Cedula;
import domain.Saque;

public class CaixaEletronicoDAO {
	
	private FactoryBuilder factoryBuilder = new FactoryBuilder();
	
	public void save(Saque saque) {
		EntityManager em = factoryBuilder.getEntityManager();
		em.getTransaction().begin();
		em.persist(saque);
		em.getTransaction().commit();
	}
	
	/**
	 * Método responsável por listar todos os saques, ordenados por data, 
	 * o primeiro registro representa o primeiro saque efetuado.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<Saque> listSaques() {
		EntityManager em = factoryBuilder.getEntityManager();
		return em.createQuery("from SAQUES order by DATA ASC").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Cedula> listTotalCedulasRetiradas() {
		EntityManager em = factoryBuilder.getEntityManager();
		List<Object> result = em.createNativeQuery("select * from SAQUE_CEDULAS order by CEDULAS").getResultList();
		
		List<Cedula> cedulas = new ArrayList<Cedula>();
		
		for (Object object : result) {
			Object[] parts = (Object[]) object;
			cedulas.add(Cedula.valueOf((String)parts[1]));
		}
		
		return cedulas;
	}
	
	public Integer getMediaDeSaques() {
		EntityManager em = factoryBuilder.getEntityManager();
		Object result = em.createNativeQuery("select avg(s.VALOR) from SAQUES as s").getSingleResult();
		return (Integer) result;
	}
	
}
