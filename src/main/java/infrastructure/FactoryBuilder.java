package infrastructure;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class FactoryBuilder {
	
	private static EntityManagerFactory entityManagerFactory;
	
	static {
        entityManagerFactory = Persistence.createEntityManagerFactory("caixa");
    }

    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}
