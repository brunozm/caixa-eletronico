package infrastructure;

import org.junit.Assert;
import org.junit.Test;

import domain.OperacoesCaixaEletronico;
import domain.Saque;

public class CaixaEletronicoDAOTest {
	
	@Test
	public void deveSalvarUmSaque() {
		OperacoesCaixaEletronico caixaEletronico = new OperacoesCaixaEletronico();
		
		Saque saque = caixaEletronico.retirar(518);
		
		saque = (Saque) new FactoryBuilder().getEntityManager().createQuery("from SAQUES").getResultList().get(0);
		Assert.assertNotNull(saque.getId());
	}

}
