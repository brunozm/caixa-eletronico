package domain;

import java.util.List;

import org.junit.Test;

import junit.framework.Assert;

public class RelatoriosCaixaEletronicoTest {
	
	@Test
	public void deveListarOsSaquesOrdenadosPorData() {
		OperacoesCaixaEletronico caixa = new OperacoesCaixaEletronico();
		RelatoriosCaixaEletronico relatorios = new RelatoriosCaixaEletronico();
		
		caixa.retirar(100);
		caixa.retirar(150);
		caixa.retirar(200);
		
		List<Saque> result = relatorios.listSaques();
		
		Assert.assertEquals(new Integer(100), result.get(0).getValor());
		Assert.assertEquals(new Integer(150), result.get(1).getValor());
		Assert.assertEquals(new Integer(200), result.get(2).getValor());
	}
	
	@Test
	public void deveListarTodasAsCedulasRetiradas() {
		OperacoesCaixaEletronico caixa = new OperacoesCaixaEletronico();
		RelatoriosCaixaEletronico relatorios = new RelatoriosCaixaEletronico();
		
		caixa.retirar(100);
		caixa.retirar(150);
		caixa.retirar(200);
		
		List<Cedula> result = relatorios.listTotalCedulasRetiradas();
		Assert.assertFalse(result.isEmpty());
	}
	
	@Test
	public void deveCalcularAMediaDoValorDasRetiradas() {
		OperacoesCaixaEletronico caixa = new OperacoesCaixaEletronico();
		RelatoriosCaixaEletronico relatorios = new RelatoriosCaixaEletronico();
		
		caixa.retirar(100);
		caixa.retirar(150);
		caixa.retirar(200);
		
		Assert.assertEquals(new Integer(150), relatorios.getMediaDeSaques());
	}

}
