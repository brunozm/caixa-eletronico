package domain;

import org.junit.Test;

import domain.exceptions.InvalidValueException;
import junit.framework.Assert;

public class OperacoesCaixaEletronicoTest {
	
	@Test
	public void deveRetornarSeisCedulas() {
		OperacoesCaixaEletronico caixa = new OperacoesCaixaEletronico();
		Saque result = caixa.retirar(550);
		
		Assert.assertEquals(6, result.getCedulas().size());
		
		int cedulasDeCinquenta = 0;
		int cedulasDeCem = 0;
		
		for (Cedula cedula : result.getCedulas()) {
			if(cedula.equals(Cedula.CINQUENTA)) {
				cedulasDeCinquenta ++;
			} else {
				cedulasDeCem ++;
			}
		}
		
		Assert.assertEquals(5, cedulasDeCem);
		Assert.assertEquals(1, cedulasDeCinquenta);
	}
	
	@Test
	public void deveRetornarQuatroCedulas() {
		OperacoesCaixaEletronico caixa = new OperacoesCaixaEletronico();
		Saque result = caixa.retirar(11);
		
		Assert.assertEquals(4, result.getCedulas().size());
		
		int cedulasDeCinco = 0;
		int cedulasDeDois = 0;
		
		for (Cedula cedula : result.getCedulas()) {
			if(cedula.equals(Cedula.CINCO)) {
				cedulasDeCinco ++;
			} else {
				cedulasDeDois ++;
			}
		}
		
		Assert.assertEquals(1, cedulasDeCinco);
		Assert.assertEquals(3, cedulasDeDois);
	}
	
	@Test
	public void deveRetornarUmaCedula() {
		OperacoesCaixaEletronico caixa = new OperacoesCaixaEletronico();
		Saque result = caixa.retirar(20);
		
		Assert.assertEquals(1, result.getCedulas().size());
		Assert.assertEquals(Cedula.VINTE, result.getCedulas().get(0));
	}
	
	@Test(expected = InvalidValueException.class)
	public void deveLancarExceptionAoTentarSacarTresReais() {
		OperacoesCaixaEletronico caixa = new OperacoesCaixaEletronico();
		caixa.retirar(3);
	}
	
	@Test(expected = InvalidValueException.class)
	public void deveLancarExceptionAoTentarSacarMaisDeMilReais() {
		OperacoesCaixaEletronico caixa = new OperacoesCaixaEletronico();
		caixa.retirar(1001);
	}

}
